/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package musicserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author adomas
 */
public class FileUpload extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FileDownload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FileDownload at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                if(new File("/tmp/uploads").mkdir())
                    throw new Exception("Nepavyko suskurti ikelimu aplanko");
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        String name = new File(item.getName()).getName();
                        File uploaded = new File("/tmp/uploads" + File.separator + name);
                        item.write(uploaded);
                        try {
                            //Process proc = Runtime.getRuntime().exec("ffmpeg -i " + uploaded.getAbsolutePath() + "-ab 320k -f mp3 -y /tmp/out.mp3 &");
                            Process proc;
                            String[] cmd = new String[] {"ffmpeg", "-i",  uploaded.getAbsolutePath(), "-ab", "320k", "-f",  "mp3", "-y",  "/tmp/out.mp3"};
                            proc = Runtime.getRuntime().exec(cmd);
                            InputStream in = proc.getErrorStream();
                            int c;
                            while ((c = in.read()) != -1) {
                                System.out.print((char) c);
                            }
                            in.close();
                            int waitFor = proc.waitFor();
                            System.out.println(cmd);
                            if (proc.exitValue() > 0) {
                                throw new Exception("nepavyko " + waitFor);
                            }
                        } catch (IOException ex) {
                            System.err.println(ex);
                        }
                    }
                }
                request.setAttribute("message", "Failas sėkmingai išsaugotas");
            } catch (Exception ex) {
                request.setAttribute("message", "Failas neišsaugotas dėl " + ex);
            }
        } else {
            request.setAttribute("message", "Galima tik įkelti failus");
        }
        request.getRequestDispatcher("/result.jsp").forward(request, response);
        System.out.println("Suveike");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
